import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {SearchResult} from '../search-result';
import {SearchService} from './search.service';
import {Router} from '@angular/router';
import {debounceTime, distinctUntilChanged, partition} from 'rxjs/operators';

const DOWN_ARROW_KEY = 40;
const UP_ARROW_KEY = 38;
const ENTER_KEY = 13;

@Component({
  selector: 'imdb-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements OnInit {
  private subject: Subject<string> = new Subject<string>();
  public results: Array<SearchResult>;
  public opened = false;
  public selectedResult = -1;
  public query = '';

  /**
   * Close search results if click outside of search results
   * @param target
   */
  @HostListener('document:click', ['$event.target'])
  onClickOutside(target) {
    if (!this.elementRef.nativeElement.contains(target)) {
      this.close();
    }
  }

  constructor(private elementRef: ElementRef,
              private searchService: SearchService,
              private router: Router) {
  }

  ngOnInit(): void {
    const observable = this.subject
      .pipe(
        debounceTime(200),
        distinctUntilChanged()
      );
    const partitionOkTooShort = partition((query: string) => query.length > 3);
    const [ok, tooShort] = partitionOkTooShort(observable);

    tooShort.subscribe(() => this.results = []);

    ok.subscribe((query: string) => {
      this.searchService.search(query)
        .subscribe((results: Array<SearchResult>) => {
          this.results = results;
          this.open();
        });
    });
  }

  onKeyUp(event: KeyboardEvent) {
    const keyCode = event.keyCode;

    if (keyCode === DOWN_ARROW_KEY) {
      // Increment index (return to 0 if overflow)
      this.selectedResult = (this.selectedResult + 1) % this.results.length;
    } else if (keyCode === UP_ARROW_KEY) {
      // Decrement index (go to list length - 1 if negative)
      this.selectedResult = (this.selectedResult - 1);
      if (this.selectedResult < 0) {
        this.selectedResult = this.results.length - 1;
      }
    } else if (keyCode === ENTER_KEY) {
      this.goTo(this.results[this.selectedResult]);
    } else {
      this.search();
    }
  }

  search() {
    this.subject.next(this.query);
  }

  open() {
    this.opened = true;
  }

  close() {
    this.opened = false;
  }

  goTo(result: SearchResult) {
    this.selectedResult = -1;
    this.query = result.text;
    this.router.navigateByUrl(result.url)
      .then(() => this.close());
  }
}
