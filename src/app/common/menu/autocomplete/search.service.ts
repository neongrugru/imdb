import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SearchResult} from '../search-result';
import {map} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  public search(query: string): Observable<Array<SearchResult>> {
    return this.http.get(`api/search/multi?query=${query.toLowerCase()}`)
      .pipe(
        map((json: any) => json.results),
        map((results: Array<SearchResult>) => results.filter(
          result => result.media_type === 'movie' || result.media_type === 'person'
        )),
        map(json => plainToClass(SearchResult, json))
      );
  }
}

