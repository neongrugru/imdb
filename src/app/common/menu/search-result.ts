export class SearchResult {
  id: number;
  media_type: string;
  poster_path: string;
  profile_path: string;
  title: string;
  name: string;

  get text() {
    if (this.media_type === 'movie') {
      return this.title;
    }
    if (this.media_type === 'person') {
      return this.name;
    }
    console.error(`media_type is neither 'movie' nor 'person'`);
  }

  get photo() {
    if (this.media_type === 'movie') {
      return this.poster_path ? `/api/${this.poster_path}` : '/assets/nomovie.png';
    }
    if (this.media_type === 'person') {
      return this.profile_path ? `/api/${this.profile_path}` : '/assets/nophoto.png';
    }
    console.error(`media_type is neither 'movie' nor 'person'`);
  }

  get url() {
    if (this.media_type === 'movie') {
      return '/movie/' + this.id;
    }
    if (this.media_type === 'person') {
      return '/person/' + this.id;
    }
    console.error(`media_type is neither 'movie' nor 'person'`);
  }
}
