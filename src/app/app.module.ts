import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { PopularPageComponent } from './movie/popular-page/popular-page.component';

import {MovieService} from './movie/movie.service';
import {HttpClientModule} from '@angular/common/http';
import { MoviePosterComponent } from './movie/movie-poster/movie-poster.component';
import {RouterModule, Routes} from '@angular/router';
import {MoviePageComponent} from './movie/movie-page/movie-page.component';
import { MovieDetailComponent } from './movie/movie-detail/movie-detail.component';
import LocalFr from '@angular/common/locales/fr';
import {registerLocaleData} from '@angular/common';
import { MovieCreditsComponent } from './movie/movie-credits/movie-credits.component';
import { PersonComponent } from './person/person/person.component';
import { PersonPageComponent } from './person/person-page/person-page.component';
import {PersonDetailComponent} from './person/person-detail/person-detail.component';
import {MenuComponent} from './common/menu/menu.component';
import { ScrollComponent } from './common/scroll/scroll.component';
import {FormsModule} from '@angular/forms';
import {AutocompleteComponent} from './common/menu/autocomplete/autocomplete.component';

const appRoutes: Routes = [
  {
    path: 'movie',
    children: [
      {path: 'popular', component: PopularPageComponent},
      {path: ':id', component: MoviePageComponent},
    ]
  },
  {
    path: 'person',
    children: [
      {path: ':id', component: PersonPageComponent}
    ]
  },
  {path: '**', redirectTo: 'movie/popular', pathMatch: 'full'}
];

registerLocaleData(LocalFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    PopularPageComponent,
    MoviePosterComponent,
    MoviePageComponent,
    MovieDetailComponent,
    MovieCreditsComponent,
    PersonComponent,
    PersonPageComponent,
    PersonDetailComponent,
    MenuComponent,
    ScrollComponent,
    AutocompleteComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
  ],
  providers: [
    MovieService,
    {provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
