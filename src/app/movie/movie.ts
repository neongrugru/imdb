export class Movie {
  id: number;
  title: string;
  poster_path: string;
  overview: string;
  budget: number;
  release_date: Date;
  vote_average: number;
  vote_count: number;

  get poster() {
    return this.poster_path ? `/api/${this.poster_path}` : `/assets/nomovie.png`;
  }

  get movieVoteAverage() {
    return this.vote_average + '/10';
  }
}
