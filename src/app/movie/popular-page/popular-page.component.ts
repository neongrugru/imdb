import { Component, OnInit } from '@angular/core';
import {Movie} from '../movie';
import {MovieService} from '../movie.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'imdb-popular-page',
  templateUrl: './popular-page.component.html',
  styleUrls: ['./popular-page.component.css']
})
export class PopularPageComponent implements OnInit {
  public movies: Movie[];

  constructor(
    private movieService: MovieService,
    private titleService: Title,
  ) { }

  ngOnInit() {
    this.titleService.setTitle(`Les films les plus populaires`);
    this.movieService.popular()
      .subscribe(movies => {
        this.movies = movies;
      });
  }
}
