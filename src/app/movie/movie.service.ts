import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Movie} from './movie';
import {map, tap} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';
import {Credits} from '../person/credits';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) { }

  public get(id: number): Observable<Movie> {
    return this.http.get(`api/movie/${id}`)
      .pipe(
        map((json: Observable<Movie>) => plainToClass(Movie, json))
      );
  }

  public popular(): Observable<Movie[]> {
    return this.http.get(`api/movie/popular`)
      .pipe(
        map((json: any) => json.results),
        map(json => plainToClass(Movie, json)),
      );
  }

  public credits(id: number): Observable<Credits> {
    return this.http.get(`api/movie/${id}/credits`)
      .pipe(
        map((json: Observable<Movie>) => plainToClass(Credits, json))
      );
  }
}
