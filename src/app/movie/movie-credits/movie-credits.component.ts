import {Component, Input} from '@angular/core';
import {Credits} from '../../person/credits';

@Component({
  selector: 'imdb-movie-credits',
  templateUrl: './movie-credits.component.html',
  styleUrls: ['./movie-credits.component.css']
})
export class MovieCreditsComponent {
  @Input()
  credits: Credits;
}
