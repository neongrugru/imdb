import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {zip} from 'rxjs';
import {Movie} from '../movie';
import {MovieService} from '../movie.service';
import {Credits} from '../../person/credits';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'imdb-movie-page',
  templateUrl: './movie-page.component.html',
  styleUrls: ['./movie-page.component.css']
})
export class MoviePageComponent implements OnInit {
  public movie: Movie;
  public credits: Credits;

  constructor(private movieService: MovieService,
              private route: ActivatedRoute,
              private titleService: Title,
  ) { }

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        const movies$ = this.movieService.get(params.id);
        const credits$ = this.movieService.credits(params.id);
        return zip(movies$, credits$);
      })
    ).subscribe(
      ([movie, credits]: [Movie, Credits]) => {
        this.movie = movie;
        this.credits = credits;
        this.titleService.setTitle(movie.title);
      });
  }
}
