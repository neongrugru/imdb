export class Person {
  id: number;
  name: string;
  profile_path: string;
  biography: string;
  birthday: Date;
  deathday: Date;
  place_of_birth: string;

  get personPic() {
    return this.profile_path ? `/api/${this.profile_path}` : `/assets/nophoto.png`;
  }

  get info(): string {
    return '';
  }

  get personDeathDay() {
    return this.deathday ? this.deathday : ' ?? ';
  }
}
