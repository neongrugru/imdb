import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Person} from './person';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';
import {Movie} from '../movie/movie';

@Injectable({
  providedIn: 'root'
})
export class PersonServiceService {

  constructor(private http: HttpClient) {
  }

  public get(id: number): Observable<Person> {
    return this.http.get(`api/person/${id}`)
      .pipe(
        map((json: Observable<Person>) => plainToClass(Person, json))
      );
  }

  public movies(id: number): Observable<Movie[]> {
    return this.http.get(`api/discover/movie?with_cast=${id}`)
      .pipe(
        map((json: any) => json.results),
        map(json => plainToClass(Movie, json)),
        );
  }
}
