import {Component, OnInit} from '@angular/core';
import {Person} from '../person';
import {PersonServiceService} from '../person-service.service';
import {ActivatedRoute, Params} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {zip} from 'rxjs';
import {Movie} from '../../movie/movie';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'imdb-person-page',
  templateUrl: './person-page.component.html',
  styleUrls: ['./person-page.component.css']
})
export class PersonPageComponent implements OnInit {
  public person: Person;
  public movie: Movie[];

  constructor(private personService: PersonServiceService,
              private route: ActivatedRoute,
              private titleService: Title,
  ) { }

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        const person$ = this.personService.get(params.id);
        const movie$ = this.personService.movies(params.id);
        return zip(person$, movie$);
    })
    ).subscribe(
      ([person, movie]: [Person, Movie[]]) => {
        this.person = person;
        this.movie = movie;
        this.titleService.setTitle(person.name);
      }
    );
  }
}
