import {Person} from './person';

export class Cast extends Person {
  character: string;

  get info() {
    return this.character;
  }
}
