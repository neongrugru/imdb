import {Person} from './person';

export class Crew extends Person {
  job: string;
  department: string;

  get info() {
    return this.job;
  }
}
