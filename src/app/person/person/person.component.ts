import {Component, Input} from '@angular/core';
import {Person} from '../person';
import {Movie} from '../../movie/movie';

@Component({
  selector: 'imdb-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent {

  @Input()
  person: Person;
  movie: Movie;
}
