import {Cast} from './cast';
import {Type} from 'class-transformer';
import {Crew} from './crew';

export class Credits {
  @Type(() => Cast)
  cast: Cast[];
  @Type(() => Crew)
  crew: Crew[];
}
